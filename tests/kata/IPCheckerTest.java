package kata;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IPCheckerTest {

    private IPChecker ipChecker;

    @BeforeEach
    void setUp() {
        ipChecker = new IPChecker();
    }

    @Test
    public void isValidIP4Address_validInput_returnsTrue() {
        String validIpAddress1 = "255.232.125.25";
        String validIpAddress2 = "123.45.67.89";
        String validIPAddress3 = ("1.2.3.4");
        boolean expected = true;

        boolean actual1 = ipChecker.isValidIP4Address(validIpAddress1);
        boolean actual2 = ipChecker.isValidIP4Address(validIpAddress2);
        boolean actual3 = ipChecker.isValidIP4Address(validIPAddress3);

        assertEquals(expected, actual1);
        assertEquals(expected, actual2);
        assertEquals(expected, actual3);
    }

    @Test
    public void isValidIP4Address_invalidInput_returnsFalse() {
        String invalidIPAddress1 = "1.2.3";
        String invalidIPAddress2 = "1.2.3.4.5";
        String invalidIPAddress3 = "123.456.78.90";
        String invalidIPAddress4 = "123.045.067.089";
        boolean expected = false;

        boolean actual1 = ipChecker.isValidIP4Address(invalidIPAddress1);
        boolean actual2 = ipChecker.isValidIP4Address(invalidIPAddress2);
        boolean actual3 = ipChecker.isValidIP4Address(invalidIPAddress3);
        boolean actual4 = ipChecker.isValidIP4Address(invalidIPAddress4);

        assertEquals(expected, actual1);
        assertEquals(expected, actual2);
        assertEquals(expected, actual3);
        assertEquals(expected, actual4);
    }
}