package kata;

import java.util.Arrays;

public class IPChecker {
    public boolean isValidIP4Address(String ipAddress) {
        // check for alphabetic characters
        if (ipAddress.toLowerCase().equals(ipAddress) == false) {
            return false;
        }
        // check if it is too long
        if (ipAddress.length() > 15) return false;

        // check if it is too short
        if (ipAddress.length() < 6) return false;

        // check each number seperated by dot
        String[] splitIpAddress = ipAddress.split("\\.");

        // check if there were more than 3 dots
        if (splitIpAddress.length > 4) return false;

        for (int index = 0; index < splitIpAddress.length; index++) {
            String numberString = splitIpAddress[index];
            // check for leading zero
            if(numberString.charAt(0) == '0') return false;

            // check number boundaries
            int number = Integer.parseInt(numberString);
            if (number < 0 || number > 255) {
                return false;
            }

            // check last character is not 0
            if (splitIpAddress.length - 1 == index) {
                if (number == 0) {
                    return false;
                }
            }
        }

        return true; // if reached, it should be valid
    }
}
